from bs4 import BeautifulSoup
import os, sys
import urllib
import nltk
from nltk.corpus import names
import numpy as np
import random
import pickle
import json
from nltk.metrics import *
import nltk.metrics




training_text = []
training_labels = []
label_names = ['Headline', 'Article_Body','Description']

i=0;

for n in label_names:
    i=i+1;

print('i' + str(i))

all_headlines = []
split_headlines = []

all_body = []
split_body = []



for f in os.listdir("nltk-naivebayes-whole/test"):
    soup = BeautifulSoup(open("nltk-naivebayes-whole/test/" + f, encoding='latin-1'), "html.parser")
    allh1 = soup.find_all("h1")
    allbody = soup.find_all("div", {'itemprop' : 'articleBody'})
    alldesc = soup.find_all("h2", {'itemprop' : 'description'})
    for h in allh1:
        all_headlines.append(h.get_text())

    for b in allbody:
        all_body.append(b.get_text())

headline_words = 0;

for headline in all_headlines:
    for word in (headline.split()):
        split_headlines.append(word)
        headline_words+=1

print(split_body)

print('Words: ' + str(headline_words))

for body in all_body:
    for word in body.split():
            split_body.append(word)

print(split_body)


#shuffle words from articlebodies
random.shuffle(split_body)

print(split_body)


split_body_sr = []
i=0

for word in split_body:
    if i < headline_words:
        split_body_sr.append(word)
        i+=1
    else:
        break


labeled_words = [({word: 'True'}, 'headline') for word in split_headlines] + [({word: 'True'}, 'no_headline') for word in split_body_sr]

random.shuffle(labeled_words)
print('labeled_words: ')
print(len(labeled_words))


#75-25
# train_set = labeled_words[:(3*len(labeled_words)//4)]
# test_set = labeled_words[(3*len(labeled_words)//4):]

#50-50
train_set = labeled_words[:len(labeled_words)//2]
test_set = labeled_words[len(labeled_words)//2:]

train_set_json = train_set
test_set_json = test_set

# convert to string
ref = json.dumps(train_set_json)
t   = json.dumps(test_set_json)

reference = set(ref)
test = set(t)


print('#########################DATEN##############################\n')
print('Trainingset Length: ')
print(len(train_set))
print('Testset Length: ')
print(len(test_set))
print('#############################################################\n\n')


classifierb = nltk.NaiveBayesClassifier.train(train_set)

print("\n\n")
print('################# MOST INFORMATIVE FEATURES ###################\n')
classifierb.show_most_informative_features()
print("\n\n")
print('######################### ACCURACY #############################\n')
print('Accuracy: ')
print(nltk.classify.accuracy(classifierb, test_set))
print('######################### Precision #############################\n')
print('Precision: ')
print(nltk.precision(reference, test))

print('######################### RECALL #############################\n')
print('Recall: ')
print(recall(reference,test))

def save_classifier(_classifier):
    f = open('nltk_naivebayes-standard.pickle', 'wb')
    pickle.dump(_classifier, f)
    f.close()

def save_datasets(_train_set, _test_set):
    f = open('nltk_naivebayes-train.pickle', 'wb')
    pickle.dump(_train_set, f)
    f.close()
    f = open('nltk_naivebayes-test.pickle', 'wb')
    pickle.dump(_test_set, f)
    f.close()




def getDict(text):
    return dict([(word, True) for word in text.split()])
