from bs4 import BeautifulSoup
import os, sys
import urllib
import nltk
from nltk.corpus import names
import numpy as np
import random
import pickle
import string

# Feature Extraction
def get_features_punc_count(_sentence):
    feature = {}
    count=0
    result=''
    found = False
    for char in _sentence:
        if char in string.punctuation:
            found = True
            result+=char
    if found==False:
        result='none'
    feature["puncs"] = result
    for word in _sentence.split():
        count+=1
    feature["wordcount"] = count
    return feature

# Load test dataset
f_data = open('./RSch3/multiple_features/nb_multi-test.pickle', 'rb')
test_set = pickle.load(f_data)
f_data.close()

# Load classifier
f_classifier = open('./RSch3/multiple_features/nb_multi-standard.pickle', 'rb')
classifier = pickle.load(f_classifier)
f_classifier.close()


training_text = []
training_labels = []


all_headlines = []
split_headlines = []

all_body = []
split_body = []

# 18 Tote bei Islamistenangriff in Somalia

i=0
#
# for f in os.listdir("./test"):
#         soup = BeautifulSoup(open("./test/" + f, encoding='latin-1'), "html.parser")
#         [s.extract() for s in soup('script')]
#         body = soup.find_all("body")
#         i+=1

soup = BeautifulSoup(open("./standard_html/18-Tote-bei-Islamistenangriff-in-Somalia.html", encoding='latin-1'), "html.parser")
[s.extract() for s in soup('script')]
# body = soup.find_all("body")
body = soup.find_all(text=True)

strings = []

for b in body:
    print('#####################################')
    print('#####################################')
    print('#####################################')
    strings.append(b)

for s in strings:
    if(len(s.split())!=1):
        print(s)
        print(classifier.classify(get_features_punc_count(s)))

print(len(strings))
