import nltk
import pickle
import string

# Feature Extraction
def get_features_punc_count(_sentence):
    feature = {}
    count=0
    result=''
    found = False
    for char in _sentence:
        if char in string.punctuation:
            found = True
            result+=char
    if found==False:
        result='none'
    feature["puncs"] = result
    for word in _sentence.split():
        count+=1
    feature["wordcount"] = count
    return feature

# Load test dataset
f_data = open('./nb_multi-test.pickle', 'rb')
test_set = pickle.load(f_data)
f_data.close()

# Load classifier
f_classifier = open('./nb_multi-standard.pickle', 'rb')
classifier = pickle.load(f_classifier)
f_classifier.close()

# Test classification
print("\n\n")
print('################# MOST INFORMATIVE FEATURES ###################\n')
classifier.show_most_informative_features()
print("\n\n")
print('######################### ACCURACY #############################\n')
print('Accuracy: ')
print(nltk.classify.accuracy(classifier, test_set))

print('\n')
print('[Warum so viele Österreicher wie nie zuvor ihr Land den Bach hinunter gehen sehen] ist ' + classifier.classify(get_features_punc_count('Warum so viele Österreicher wie nie zuvor ihr Land den Bach hinunter gehen sehen')))
print('\n')
print('[Casual Dating: Der perfekte Single-Lifestyle für unsere Zeit] ist ' + classifier.classify(get_features_punc_count('Casual Dating: Der perfekte Single-Lifestyle für unsere Zeit')))
print('\n')
print('[Zschäpe-Gutachten: "Verbergen, verschleiern, täuschen"] ist ' + classifier.classify(get_features_punc_count('Zschäpe-Gutachten: "Verbergen, verschleiern, täuschen"')))
print('\n')
print('[Diese Neonazis können wir aushalten] ist ' + classifier.classify(get_features_punc_count('Diese Neonazis können wir aushalten')))
print('\n')
print('[Konservativer Kandidat Tajani ist neuer EU-Parlamentspräsident] ist ' + classifier.classify(get_features_punc_count('Konservativer Kandidat Tajani ist neuer EU-Parlamentspräsident')))
print('\n')
print('[Der Brexit wird existenziell für die EU] ist ' + classifier.classify(get_features_punc_count('Der Brexit wird existenziell für die EU')))
print('\n')
print('[Weltweit wurden am Samstag Großproteste gegen die Amtsübernahme von US-Präsident Donald Trump abgehalten.] ist ' + classifier.classify(get_features_punc_count('Weltweit wurden am Samstag Großproteste gegen die Amtsübernahme von US-Präsident Donald Trump abgehalten.')))
print('\n')
print('[Laut Schätzungen nahmen an den Protesten mehr als 2,5 Millionen Menschen teil, die Zahlen wurden offiziell nicht bestätigt.] ist ' + classifier.classify(get_features_punc_count('Laut Schätzungen nahmen an den Protesten mehr als 2,5 Millionen Menschen teil, die Zahlen wurden offiziell nicht bestätigt.')))
print('\n')
print('[Der neue Präsident bezichtigte die Medien abermals, unfair über ihn zu berichten.] ist ' + classifier.classify(get_features_punc_count('Der neue Präsident bezichtigte die Medien abermals, unfair über ihn zu berichten.')))
print('\n')
print('[Dabei sei "genau das Gegenteil der Fall".] ist ' + classifier.classify(get_features_punc_count('Dabei sei "genau das Gegenteil der Fall".')))
print('\n')
print('[Dass er mit der CIA und anderen Diensten über Kreuz gelegen habe, tat er als Erfindung der Medien ab.] ist ' + classifier.classify(get_features_punc_count('Dass er mit der CIA und anderen Diensten über Kreuz gelegen habe, tat er als Erfindung der Medien ab.')))
print('\n')
print('[Die Enthüllungsplattform Wikileaks hatte während des Wahlkampfs E-Mails der Demokratischen Partei und von Clintons Wahlkampfmanager John Podesta veröffentlicht.] ist ' + classifier.classify(get_features_punc_count('Die Enthüllungsplattform Wikileaks hatte während des Wahlkampfs E-Mails der Demokratischen Partei und von Clintons Wahlkampfmanager John Podesta veröffentlicht.')))
print('\n')
print('[Polizei nimmt Terrorverdächtigen in NRW fest] ist ' + classifier.classify(get_features_punc_count('Polizei nimmt Terrorverdächtigen in NRW fest')))
print('\n')
print('[Er soll einen Anschlag auf Bundeswehrsoldaten geplant haben.] ist ' + classifier.classify(get_features_punc_count('Er soll einen Anschlag auf Bundeswehrsoldaten geplant haben.')))
print('\n')
print('[Es seien auch Computer, Handys und Datenträger beschlagnahmt worden.] ist ' + classifier.classify(get_features_punc_count('Es seien auch Computer, Handys und Datenträger beschlagnahmt worden.')))
print('\n')
