######     #    #######    ######   #####   #####  #     #  #####  
#     #   # #   #          #     # #     # #     # #     # #     #
#     #  #   #  #          #     # #       #       #     #       #
######  #     # #####      ######   #####  #       #######  #####  
#       ####### #          #   #         # #       #     #       #
#       #     # #          #    #  #     # #     # #     # #     #
#       #     # #######    #     #  #####   #####  #     #  #####  




Use the IPython Notebook in the folder ipython_notebooks for
looking at the whole process of our script, from getting the data
to training the classifier and testing it (may take some time to
execute). If you only wish to execute tests, you can only execute the
notebooks for testing as the classifier and data are serialized and
ready to use.
In the textblob directory, the textblob implementation can be found
as an IPython notebook as well.
We have also included our source files for the implementation of mile-
stone 1 in the src-directory and the source files for the final imple-
mentation in the two final_* directories. If you wish to execute them
locally, you have to put the html-files in the standard_html (inside
nltk-naivebayes-whole) folder into the right place or change the path
directly in the script.

All scripts written in Python 3.5.

__________________________________________________________________


1. git clone https://gitlab.com/rjaz/RSch3
2. pip install jupyter
3. pip install textblob
4. pip install nltk
5. pip install beautifulsoup4
6. cd to desired directory containing .ipynb files
7. ipython notebook (or alternatively run scripts)

__________________________________________________________________
