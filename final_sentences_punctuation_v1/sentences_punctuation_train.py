from bs4 import BeautifulSoup
import os, sys
import urllib
import nltk
from nltk.corpus import names
import numpy as np
import random
import pickle
import string

training_text = []
training_labels = []

all_headlines = []
split_headlines = []

all_body = []
split_body = []

def save_classifier(_classifier):
    f = open('nb_ps-standard.pickle', 'wb')
    pickle.dump(_classifier, f)
    f.close()

def save_datasets(_train_set, _test_set):
    f = open('nb_ps-train.pickle', 'wb')
    pickle.dump(_train_set, f)
    f.close()
    f = open('nb_ps-test.pickle', 'wb')
    pickle.dump(_test_set, f)
    f.close()


for f in os.listdir("../nltk-naivebayes-whole/standard_html"):
    soup = BeautifulSoup(open("../nltk-naivebayes-whole/standard_html/" + f, encoding='latin-1'), "html.parser")
    allh1 = soup.find_all("h1")
    allbody = soup.find_all("div", {'itemprop' : 'articleBody'})
    alldesc = soup.find_all("h2", {'itemprop' : 'description'})
    for h in allh1:
        all_headlines.append(h.get_text())

    for b in allbody:
        all_body.append(b.get_text())

headline_words = 0;

for headline in all_headlines:
    found = False
    result = ''
    for char in headline:
        if char in string.punctuation:
            result+=char
            found = True
            headline_words+=1
    if found==False:
        result="none"
    split_headlines.append(result)


print(split_headlines)
# print('Words: ' + str(headline_words))



split_body_sr = []
i=0

for body in all_body:
    for sentence in body.split('.'):
        result=''
        found = False
        sentence+='.'
        if i < headline_words:
            for char in sentence:
                if char in string.punctuation:
                    i+=1
                    result+=char
                    found = True
        else:
            break
        if found==False:
            split_body_sr.append("none")
        split_body_sr.append(result)



labeled_sequences = [({sequence: 'True'}, 'headline') for sequence in split_headlines] + [({sequence: 'True'}, 'no_headline') for sequence in split_body_sr]

random.shuffle(labeled_sequences)


#75-25
# train_set = labeled_words[:(3*len(labeled_words)//4)]
# test_set = labeled_words[(3*len(labeled_words)//4):]

#50-50
train_set = labeled_sequences[:len(labeled_sequences)//2]
test_set = labeled_sequences[len(labeled_sequences)//2:]


print('#########################DATEN##############################\n')
print("\n")
print('Trainingset Length: ')
print(len(train_set))
print("\n")
print('Testset Length: ')
print(len(test_set))
print("\n")
print('#############################################################\n\n')


classifierb = nltk.NaiveBayesClassifier.train(train_set)

print("\n\n")
print('################# MOST INFORMATIVE FEATURES ###################\n')
print("\n")
classifierb.show_most_informative_features()
print("\n\n")
print('######################### ACCURACY #############################\n')
print("\n")
print('Accuracy: ')
print(nltk.classify.accuracy(classifierb, test_set))


save_classifier(classifierb)
save_datasets(train_set, test_set)


def getDict(text):
    return dict([(word, True) for word in text.split()])
