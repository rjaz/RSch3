from bs4 import BeautifulSoup
import os, sys
import urllib
import random
import pickle


all_headlines = []
split_headlines = []

all_body = []
split_body = []


# Parse html-files and append text of each headline and articlebody to respective array
for f in os.listdir("./standard_html"):
    soup = BeautifulSoup(open("./standard_html/" + f, encoding='latin-1'), "html.parser")
    allh1 = soup.find_all("h1")
    allbody = soup.find_all("div", {'itemprop' : 'articleBody'})

    for h in allh1:
        all_headlines.append(h.get_text())

    for b in allbody:
        all_body.append(b.get_text())

# Split all headlines by " " (single words) and append them to new headline array
for headline in all_headlines:
    for word in (headline.split()):
        split_headlines.append(word)

# Split all article bodies by " " (single words) and append them to new articlebody array
for body in all_body:
    for word in (body.split()):
        split_body.append(word)

# Create array with all words labeled from headlines and article bodies in correct format for nltk naive bayes classifier
labeled_words = [({word: 'True'}, 'headline') for word in split_headlines] + [({word: 'True'}, 'no_headline') for word in split_body]

# Shuffle array for splitting it up in training- and test-data
random.shuffle(labeled_words)

# Split array in half (50:50 training:testing for now)
train_set = labeled_words[:len(labeled_words)//2]
test_set = labeled_words[len(labeled_words)//2:]

# Serialize datasets
def save_datasets(_train_set, _test_set):
    f = open('./nltk_naivebayes-train_set.pickle', 'wb')
    pickle.dump(_train_set, f)
    f.close()
    f = open('./nltk_naivebayes-test_set.pickle', 'wb')
    pickle.dump(_test_set, f)
    f.close()

save_datasets(train_set, test_set)
