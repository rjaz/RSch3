import nltk
import pickle


# Load training dataset
f = open('./nltk_naivebayes-train_set.pickle', 'rb')
train_set = pickle.load(f)
f.close()

# Save classifier
def save_classifier(_classifier):
    f = open('nltk_naivebayes-standard.pickle', 'wb')
    pickle.dump(_classifier, f)
    f.close()

# Create NaiveBayesClassifier
classifier = nltk.NaiveBayesClassifier.train(train_set)

save_classifier(classifier)
