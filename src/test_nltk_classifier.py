import nltk
import pickle


# Load test dataset
f_data = open('./nltk_naivebayes-test_set.pickle', 'rb')
test_set = pickle.load(f_data)
f_data.close()

# Load classifier
f_classifier = open('./nltk_naivebayes-standard.pickle', 'rb')
classifier = pickle.load(f_classifier)
f_classifier.close()

# Test classification
print("\n\n")
print('################# MOST INFORMATIVE FEATURES ###################\n')
classifier.show_most_informative_features()
print("\n\n")
print('######################### ACCURACY #############################\n')
print('Accuracy: ')
print(nltk.classify.accuracy(classifier, test_set))
