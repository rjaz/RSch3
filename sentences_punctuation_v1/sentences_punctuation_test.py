import nltk
import pickle
import string

# Feature Extraction
def get_features_punc(_sentence):
    result=''
    found = False
    for char in _sentence:
        if char in string.punctuation:
            found = True
            result+=char
    if found==False:
        result='none'
    return {result: 'True'}

# Load test dataset
f_data = open('./nb_ps-test.pickle', 'rb')
test_set = pickle.load(f_data)
f_data.close()

# Load classifier
f_classifier = open('./nb_ps-standard.pickle', 'rb')
classifier = pickle.load(f_classifier)
f_classifier.close()

# Test classification
print("\n\n")
print('################# MOST INFORMATIVE FEATURES ###################\n')
classifier.show_most_informative_features()
print("\n\n")
print('######################### ACCURACY #############################\n')
print('Accuracy: ')
print(nltk.classify.accuracy(classifier, test_set))

print('\n')
print('[Warum so viele Österreicher wie nie zuvor ihr Land den Bach hinunter gehen sehen] ist ' + classifier.classify(get_features_punc('Warum so viele Österreicher wie nie zuvor ihr Land den Bach hinunter gehen sehen')))
print('\n')
print('[Casual Dating: Der perfekte Single-Lifestyle für unsere Zeit] ist ' + classifier.classify(get_features_punc('Casual Dating: Der perfekte Single-Lifestyle für unsere Zeit')))
print('\n')
print('[Zschäpe-Gutachten: "Verbergen, verschleiern, täuschen"] ist ' + classifier.classify(get_features_punc('Zschäpe-Gutachten: "Verbergen, verschleiern, täuschen"')))
print('\n')
print('[Diese Neonazis können wir aushalten] ist ' + classifier.classify(get_features_punc('Diese Neonazis können wir aushalten')))
print('\n')
print('[Konservativer Kandidat Tajani ist neuer EU-Parlamentspräsident] ist ' + classifier.classify(get_features_punc('Konservativer Kandidat Tajani ist neuer EU-Parlamentspräsident')))
print('\n')
print('[Der Brexit wird existenziell für die EU] ist ' + classifier.classify(get_features_punc('Der Brexit wird existenziell für die EU')))
print('\n')
print('[Weltweit wurden am Samstag Großproteste gegen die Amtsübernahme von US-Präsident Donald Trump abgehalten.] ist ' + classifier.classify(get_features_punc('Weltweit wurden am Samstag Großproteste gegen die Amtsübernahme von US-Präsident Donald Trump abgehalten.')))
print('\n')
print('[Laut Schätzungen nahmen an den Protesten mehr als 2,5 Millionen Menschen teil, die Zahlen wurden offiziell nicht bestätigt.] ist ' + classifier.classify(get_features_punc('Laut Schätzungen nahmen an den Protesten mehr als 2,5 Millionen Menschen teil, die Zahlen wurden offiziell nicht bestätigt.')))
print('\n')
print('[Der neue Präsident bezichtigte die Medien abermals, unfair über ihn zu berichten.] ist ' + classifier.classify(get_features_punc('Der neue Präsident bezichtigte die Medien abermals, unfair über ihn zu berichten.')))
print('\n')
print('[Dabei sei "genau das Gegenteil der Fall".] ist ' + classifier.classify(get_features_punc('Dabei sei "genau das Gegenteil der Fall".')))
print('\n')
print('[Dass er mit der CIA und anderen Diensten über Kreuz gelegen habe, tat er als Erfindung der Medien ab.] ist ' + classifier.classify(get_features_punc('Dass er mit der CIA und anderen Diensten über Kreuz gelegen habe, tat er als Erfindung der Medien ab.')))
print('\n')
print('[Die Enthüllungsplattform Wikileaks hatte während des Wahlkampfs E-Mails der Demokratischen Partei und von Clintons Wahlkampfmanager John Podesta veröffentlicht.] ist ' + classifier.classify(get_features_punc('Die Enthüllungsplattform Wikileaks hatte während des Wahlkampfs E-Mails der Demokratischen Partei und von Clintons Wahlkampfmanager John Podesta veröffentlicht.')))
print('\n')
print('[Polizei nimmt Terrorverdächtigen in NRW fest] ist ' + classifier.classify(get_features_punc('Polizei nimmt Terrorverdächtigen in NRW fest')))
print('\n')
print('[Er soll einen Anschlag auf Bundeswehrsoldaten geplant haben.] ist ' + classifier.classify(get_features_punc('Er soll einen Anschlag auf Bundeswehrsoldaten geplant haben.')))
print('\n')
print('[Es seien auch Computer, Handys und Datenträger beschlagnahmt worden.] ist ' + classifier.classify(get_features_punc('Es seien auch Computer, Handys und Datenträger beschlagnahmt worden.')))
print('\n')
