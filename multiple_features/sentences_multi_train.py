from bs4 import BeautifulSoup
import os, sys
import urllib
import nltk
from nltk.corpus import names
import numpy as np
import random
import pickle
import string


training_text = []
training_labels = []


all_headlines = []
split_headlines = []

all_body = []
split_body = []

def get_features_punc(_sentence):
    result=''
    found = False
    for char in _sentence:
        if char in string.punctuation:
            found = True
            result+=char
    if found==False:
        result='none'
    return {result: 'True'}


def save_classifier(_classifier):
    f = open('nb_multi-standard.pickle', 'wb')
    pickle.dump(_classifier, f)
    f.close()

def save_datasets(_train_set, _test_set):
    f = open('nb_multi-train.pickle', 'wb')
    pickle.dump(_train_set, f)
    f.close()
    f = open('nb_multi-test.pickle', 'wb')
    pickle.dump(_test_set, f)
    f.close()



for f in os.listdir("./standard_html"):
    soup = BeautifulSoup(open("./standard_html/" + f, encoding='latin-1'), "html.parser")
    allh1 = soup.find_all("h1")
    allbody = soup.find_all("div", {'itemprop' : 'articleBody'})
    alldesc = soup.find_all("h2", {'itemprop' : 'description'})
    for h in allh1:
        all_headlines.append(h.get_text())

    for b in allbody:
        all_body.append(b.get_text())

headline_words = 0;

for headline in all_headlines:
    feature = {}
    found = False
    count = 0
    result = ''
    for char in headline:
        if char in string.punctuation:
            result+=char
            found = True
            headline_words+=1
    if found==False:
        result="none"

    feature["puncs"] = result

    for word in headline.split():
        count+=1

    feature["wordcount"] = count
    split_headlines.append(feature)



# print('Words: ' + str(headline_words))



split_body_sr = []
i=0

for body in all_body:
    for sentence in body.split('.'):
        feature = {}
        count=0
        result=''
        found = False
        sentence+='.'
        if i < headline_words:
            for char in sentence:
                if char in string.punctuation:
                    i+=1
                    result+=char
                    found = True
        else:
            break
        if found==False:
            result=="none"

        feature["puncs"] = result

        for word in sentence.split():
            count+=1

        feature["wordcount"] = count
        split_body_sr.append(feature)



labeled_sequences = [(sequence, 'headline') for sequence in split_headlines] + [(sequence, 'no_headline') for sequence in split_body_sr]

random.shuffle(labeled_sequences)


#75-25
# train_set = labeled_words[:(3*len(labeled_words)//4)]
# test_set = labeled_words[(3*len(labeled_words)//4):]

#50-50
train_set = labeled_sequences[:len(labeled_sequences)//2]
test_set = labeled_sequences[len(labeled_sequences)//2:]


print('#########################DATEN##############################\n')
print('Trainingset Length: ')
print(len(train_set))
print('Testset Length: ')
print(len(test_set))
print('#############################################################\n\n')


classifierb = nltk.NaiveBayesClassifier.train(train_set)

print("\n\n")
print('################# MOST INFORMATIVE FEATURES ###################\n')
classifierb.show_most_informative_features()
print("\n\n")
print('######################### ACCURACY #############################\n')
print('Accuracy: ')
print(nltk.classify.accuracy(classifierb, test_set))


save_classifier(classifierb)
save_datasets(train_set, test_set)


def getDict(text):
    return dict([(word, True) for word in text.split()])
